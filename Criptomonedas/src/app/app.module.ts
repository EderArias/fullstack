import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { RouterModule, Routes } from '@angular/router'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatTableModule } from '@angular/material/table';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { ListadoComponent } from './listado/listado.component';
import { CambioComponent } from './cambio/cambio.component';

import { ListmonService } from './services/listmon.service'

const appRoutes: Routes = [
  { path: 'cambiodemoneda',  component: CambioComponent },
  { path: 'listadodemoneda',     component: ListadoComponent }
]



@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    ListadoComponent,
    CambioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatCardModule,
    MatTabsModule,
    MatGridListModule,
    //MatPaginator,
    MatButtonModule,
    FlexLayoutModule,
    //MatCheckboxModule,
    FormsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule,
    //MatTableDataSource,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatTableModule,
    MatSlideToggleModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true}

    )
  ],
  providers: [ListmonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
