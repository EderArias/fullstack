import { Routes } from '@angular/router';

import { ListadoComponent } from '../listado/listado.component';
import { CambioComponent } from '../cambio/cambio.component';

export const routes: Routes = [
  { path: 'cambiodemoneda',  component: CambioComponent },
  { path: 'listadodemoneda',     component: ListadoComponent }
  
];