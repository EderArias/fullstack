import { Component, OnInit } from '@angular/core';
import { Moneda } from '../shared/moneda';
import { ListmonService } from '../services/listmon.service';
@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss']
})
export class ListadoComponent implements OnInit {

  monedas!: Moneda[];

  constructor(private monedaService: ListmonService,
    ) { }


    displayedColumns = ['name', 'precio', 'criptomoneda', 'convertir'];
    

  ngOnInit(): void {
    this.monedas = this.monedaService.getMonedas();
  }

}
