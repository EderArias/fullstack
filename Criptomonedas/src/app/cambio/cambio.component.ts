import { TypeScriptEmitter } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Convertir, opciones } from '../shared/formconvertir';



@Component({
  selector: 'app-cambio',
  templateUrl: './cambio.component.html',
  styleUrls: ['./cambio.component.scss']
})
export class CambioComponent implements OnInit {

  feedbackForm!: FormGroup;
  conv!: Convertir;
  Typesi = opciones;
  //Typeso = opciones;
  res!: number;
  calculo:number = this.getconversion();
  selectedvalue!: string;
  
  

  constructor(private fb: FormBuilder) { 
    this.createForm();
  }

  ngOnInit(){
  }

  createForm(){
    this.feedbackForm = this.fb.group({
      valor: 0,
      typesy: 'BTC',
      typeso: 'USD',
      calculoo: this.calculo
    });

    
  }
  
  getconversion(){
    return 0;
  }


  


}
