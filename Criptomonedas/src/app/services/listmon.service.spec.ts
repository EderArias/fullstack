import { TestBed } from '@angular/core/testing';

import { ListmonService } from './listmon.service';

describe('ListmonService', () => {
  let service: ListmonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListmonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
