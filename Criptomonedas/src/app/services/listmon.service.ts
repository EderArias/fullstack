import { Injectable } from '@angular/core';
import { Moneda } from '../shared/moneda';
import { MONEDAS } from '../shared/monedas';
@Injectable({
  providedIn: 'root'
})
export class ListmonService {

  constructor() { }

  getMonedas(): Moneda[] {
    return MONEDAS;
  }
}
