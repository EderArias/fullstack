export interface Moneda {
    name: string | undefined;
    precio: string | undefined;
    criptomoneda: string | undefined;
    convertir: string | undefined;
  
}