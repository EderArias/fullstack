import { Moneda } from './moneda';

export const MONEDAS: Moneda[] = [
    {
        name: 'AsianCoin',
        precio: '$14,000',
        criptomoneda: 'Si',
        convertir: '/assets/shuffle.png'
    },
    {
        name: '808Coin',
        precio: '$14,000',
        criptomoneda: 'Si',
        convertir: '/assets/shuffle.png'
    },
    {
        name: 'USD',
        precio: '$14,000',
        criptomoneda: 'No',
        convertir: '/assets/shuffle.png'
    },
    {
        name: 'EUR',
        precio: '$14,000',
        criptomoneda: 'No',
        convertir: '/assets/shuffle.png'
    }
]