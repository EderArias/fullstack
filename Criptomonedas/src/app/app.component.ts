import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  navLinks = [
    {path: 'cambiodemoneda', label:'REALIZAR CAMBIO'},
    {path: 'listadodemoneda', label: 'LISTADO DE MONEDAS' }
  ]
  title = 'Criptomonedas';
}
